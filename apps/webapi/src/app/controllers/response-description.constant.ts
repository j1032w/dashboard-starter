export const RESPONSE_200_DESCRIPTION = "Success.";
export const RESPONSE_201_DESCRIPTION = "Created.";

export const RESPONSE_400_DESCRIPTION = "Bad request.";
export const RESPONSE_401_DESCRIPTION = "Unauthorized.";
export const RESPONSE_403_DESCRIPTION = "Forbidden.";
export const RESPONSE_404_DESCRIPTION = "Resource not found.";

export const RESPONSE_500_DESCRIPTION = "Internal server error.";

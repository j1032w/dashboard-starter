import { Injectable } from '@angular/core';

import { DasHttpClient } from '../../../common/services/das-http-client';

@Injectable({ providedIn: 'root' })
export class DasRealEstateMarketRepository {
  constructor(private readonly dasHttpClient: DasHttpClient) {}

  query$(mongoFilter: any, spinnerId: string): any {
    return this.dasHttpClient.post$(`v1/real-estate-listings/findAll`, mongoFilter, spinnerId);
  }
}

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ChartData } from 'chart.js';
import { takeUntil } from 'rxjs';
import {DasSpinnerComponent} from '../../../common/components/das-spinner/das-spinner.component';
import { DasWidgetSettingCoreComponent } from '../../../common/components/dashboard-core/das-widget-setting/das-widget-setting-core.component';

import { DasDashboardCoreEventService } from '../../../common/components/dashboard-core/services/das-dashboard-core-event.service';
import { DasWidgetBaseComponent } from '../../../common/components/dashboard-core/services/das-widget-base.component';
import { DasWidgetCoreComponent } from '../../../common/components/dashboard-core/widget-core/das-widget-core.component';
import {DasCommonComponentModule} from '../../../common/das-common-component.module';
import { DasHttpClient } from '../../../common/services/das-http-client';
import { DasToastService } from '../../../common/services/das-toast.service';
import { DvLocWidgetPieComponent } from './loc-widget-pie/dv-loc-widget-pie.component';

@Component({
  selector: 'das-dv-loc-widget',
  templateUrl: './dv-loc-widget.component.html',
  styleUrls: ['./dv-loc-widget.component.scss'],
  standalone: true,
  imports: [DasWidgetSettingCoreComponent, DvLocWidgetPieComponent, DasWidgetCoreComponent, DasSpinnerComponent, DasCommonComponentModule]
})
export class DvLocWidgetComponent extends DasWidgetBaseComponent implements OnInit {
  @ViewChild('frontTemplate') widgetFrontComponent: ElementRef;
  @ViewChild('settingTemplate') widgetSettingComponent: ElementRef;

  spinnerId = 'loc-widget-spinner';

  total: any = {};

  pieChartData: ChartData<'pie', number[], string>;

  constructor(
    protected override readonly dashboardCoreService: DasDashboardCoreEventService,
    protected override readonly toastService: DasToastService,
    private readonly dasHttpClient: DasHttpClient
  ) {
    super(dashboardCoreService, toastService);
  }

  override ngOnInit() {
    super.ngOnInit();
    this.refresh();
  }

  protected override readonly refresh = () => {
    const url = 'https://api.codetabs.com/v1/loc/?' + 'github=j1032w/dashboard-starter&branch=main&ignored=.js';

    this.dasHttpClient
      .getExternal$(url, this.spinnerId)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(response => {
        this.total = response.find((item: any) => item.language === 'Total');

        const index = response.indexOf(this.total);
        if (index > -1) {
          response.splice(index, 1);
        }

        this.pieChartData = {
          labels: response.map((item: any) => item.language),
          datasets: [
            {
              data: response.map((item: any) => item.lines)
            }
          ]
        };
      });
  };
}
